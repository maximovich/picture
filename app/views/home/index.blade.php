@extends('layouts.main')

@section('title'){{ 'Исполнение желаний' }}@stop

@section('keywords'){{ 'Исполнение желаний' }}@stop

@section('description'){{ 'Исполнение желаний' }}@stop

@section('content')

<div id="large-header" class="large-header">
    <canvas id="demo-canvas"></canvas>
    <h1 class="main-title">
        <span id="sectionCheck1" class="sectionCheck"></span>
        <span id="sectionCheck2" class="sectionCheck"></span>
        <span id="sectionCheck3" class="sectionCheck"></span>
        <br>
        <span id="skazka">Загадай желание</span>
        <br>
        <br>
        <button id="check" class="btn btn-info">Показать вероятность исполнения</button>
        <br>
        <br>
        <span class="thin">Совпадения: 2 - 50%, 3 - 100%</span>
    </h1>
</div>
<script src="/assets/header/TweenLite.min.js"></script>
<script src="/assets/header/EasePack.min.js"></script>
<script src="/assets/header/rAF.js"></script>
<script src="/assets/header/demo-1.js"></script>
@stop



@section('scripts')
<script>
    $().ready(function(){
        $('#sectionCheck1').hide();
        $('#sectionCheck2').hide();
        $('#sectionCheck3').hide();

        $('#check').click(function(e){
           e.preventDefault();
           $('#check').hide();
           $('#skazka').hide();

           $('#sectionCheck1').show();
           $('#sectionCheck2').show();
           $('#sectionCheck3').show();


            var min=0;
            var max = 9;

            var timer = 0;
            var checkTimer = 20;

            var result = setInterval(function(){
                if(timer>checkTimer){
                    clearInterval(result);
                    $('#check').show();
                    $('#skazka').show().html('Загадать еще одно');
                    timer=0;
                }

                $('#sectionCheck1').html(Math.floor(Math.random() * (max - min + 1)) + min);
                $('#sectionCheck2').html(Math.floor(Math.random() * (max - min + 1)) + min);
                $('#sectionCheck3').html(Math.floor(Math.random() * (max - min + 1)) + min);
                if($('#sectionCheck1').html()==$('#sectionCheck2').html() || $('#sectionCheck2').html()==$('#sectionCheck3' ).html() || $('#sectionCheck1').html()==$('#sectionCheck2').html()){
                    var bingo = Math.floor(Math.random() * (max - min + 1)) + min;
                    $('#sectionCheck1').html(bingo);
                    $('#sectionCheck2').html(bingo);
                    $('#sectionCheck3').html(bingo);
                }
                timer++;
            }, 80);

        });
    });
</script>
@stop