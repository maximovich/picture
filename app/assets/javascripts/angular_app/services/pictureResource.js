'use strict';

pictureServices.factory('pictureResource', ['$resource', function ($resource) {
    return function (url, params, methods) {
        var defaults = {
            query: {
                isArray: false
            }
        };

        methods = angular.extend(defaults, methods);

        var resource = $resource(url, params, methods);

        return resource;
    };
}]);