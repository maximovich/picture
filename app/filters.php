<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(
    function ( $request ) {
        //
    }
);


App::after(
    function ( $request, $response ) {
        //
    }
);

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter(
    'auth',
    function () {
        if ( Auth::guest() ) {
            if ( Request::ajax() ) {
                return Response::make( 'Unauthorized', 401 );
            } else {
                return Redirect::guest( 'login' );
            }
        }
    }
);


Route::filter(
    'auth.basic',
    function () {
        return Auth::basic();
    }
);

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter(
    'guest',
    function () {
        if ( !Sentry::check() ) {
            return Redirect::to( '/' );
        }
    }
);

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter(
    'csrf',
    function () {
        if ( Session::token() != Input::get( '_token' ) ) {
            throw new Illuminate\Session\TokenMismatchException;
        }
    }
);

Route::filter(
    'user',
    function () {

        if ( (int)Input::get( 'viewer_id' ) ) {
            $auth_key = hash( 'md5', '4569339_' . Input::get( 'viewer_id' ) . '_hyquuJZXdn7kidSK4tSi' );

            if ( $auth_key != Input::get( 'auth_key' ) ) {
                echo 'Ты хакер что ли?';
                die();
            }
            if ( Sentry::check() && (int)Input::get( 'viewer_id' ) != Sentry::getUser()->vk->id ) {
                Sentry::logout();
            }
        }


        if ( !Sentry::check() ) {

            if ( !$vkUserId = (int)Input::get( 'viewer_id' ) ) {
                echo 'Ошибка 1, попробуйте перезагрузить приложение';
                die();
            }

            $vk = Vk::find( $vkUserId );

            if ( !$vk ) {
                try {
                    $user = Sentry::createUser(
                        [
                            'username'  => $vkUserId,
                            'password'  => $vkUserId . str_random( 10 ),
                            'email'     => $vkUserId . '@example.com',
                            'activated' => true,
                        ]
                    );
                } catch ( Exception $e ) {
                    echo 'Ошибка 2, попробуйте перезагрузить приложение';
                    die();
                }
                if ( !$vk = Vk::build( $vkUserId, $user ) ) {
                    echo 'Ошибка 3, попробуйте перезагрузить приложение';
                    die();
                }
            }
            try {
                Sentry::loginAndRemember( $vk->user );
            } catch ( Exception $e ) {
                echo 'Ошибка 4, попробуйте перезагрузить приложение';
                die();
            }
        } else {

        }
    }
);
