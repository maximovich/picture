<?php

use LaravelBook\Ardent\Ardent;

class Vk extends Ardent
{
    protected $table = 'vk';

    public function user()
    {
        return $this->belongsTo( 'User' );
    }

    public static function build( $vkId, $user )
    {
        $vk     = new Vk;
        $vk->id = $vkId;
        $vk->user()->associate( $user );
        if ( !$vk->save() ) {
            return false;
        }
        return $vk;
    }
}