<?php

namespace Api\v1;

use App, Input;
use User;

class UsersController extends ApiController
{

    public function update( $userId )
    {
        if ( !Input::get( 'first_name' ) && !Input::get( 'last_name' ) ) {
            App::abort( 500 );
        }

        $user = User::findOrFail( $userId );

        $user->vk->first_name = Input::get( 'first_name' );
        $user->vk->last_name  = Input::get( 'last_name' );
        $user->vk->save();
    }

}
