<?php


namespace Picture\Serializer;

use League\Fractal\Serializer\DataArraySerializer as BaseDataArraySerializer;

class DataArraySerializer extends BaseDataArraySerializer
{

    public function collection( $resourceKey, array $data )
    {
        if ( $resourceKey ) {
            return array( $resourceKey => $data );
        }
        return $data;
    }

    public function item( $resourceKey, array $data )
    {
        if ( $resourceKey ) {
            return array( $resourceKey => $data );
        }

        return $data;
    }
}