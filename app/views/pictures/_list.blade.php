@if($pictures->count())
    <div>
        <div class="row">
                    <div class="col-xs-12">
                        {{ $pictures->links() }}
                    </div>
                </div>

        <div class="row">
        @foreach ($pictures as $picture)
            <div class="col-xs-4" style="min-height: 400px;">
                <div>
                    <a href="{{route('pictures.show', [$picture->id])}}">
                        <img class="img-responsive" src="{{'/'.$picture->path}}">
                    </a>
                </div>
                <div>
                    Разместил: <a target="_blank" href="http://vk.com/id{{$picture->user->vk->id}}">{{$picture->user->vk->first_name}} {{$picture->user->vk->last_name}}</a>
                </div>
                <div class="text-right">
                    <a class="like" ng-href="" ng-click="like({{$picture->id}})">Мне нравиться</a>
                    <span class="glyphicon glyphicon-heart"></span>
                    <span id="picture{{$picture->id}}">
                        {{$picture->likes()->count() ? $picture->likes()->count() : ''}}
                    </span>
                </div>
                <hr>
            </div>
        @endforeach
        </div>

        <div class="row">
            <div class="col-xs-12">
                {{ $pictures->links() }}
            </div>
        </div>

    </div>
@else
    Ничего нет
@endif