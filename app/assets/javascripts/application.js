// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear in whatever order it 
// gets included (e.g. say you have require_tree . then the code will appear after all the directories 
// but before any files alphabetically greater than 'application.js' 
//
// The available directives right now are require, require_directory, and require_tree
//
//= require ..//components/jquery/dist/jquery.js
//= require notify/notify
//= require bootstrap/bootstrap.js
//= require main.js
//= require ..//components/angular/angular.js
//= require ..//components/angular-route/angular-route
//= require ..//components/angular-resource/angular-resource
//= require ..//components/angular-bootstrap/ui-bootstrap-tpls.js
//= require_tree  angular_app