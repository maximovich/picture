<div class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            {{ link_to_route('home', 'Большие открытки', [], ['class' => 'navbar-brand']) }}
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li
                    {{ Route::currentRouteName() == 'home' ? 'class="active"' : '' }}>
                    {{ link_to_route('home', 'Исполнение желаний') }}
                </li>
                <li
                    {{ Route::currentRouteName() == 'pictures.create' ? 'class="active"' : '' }}>
                    {{ link_to_route('pictures.create', 'Новая') }}
                </li>
                <li
                    {{ Route::currentRouteName() == 'pictures.index' ? 'class="active"' : '' }}>
                    {{ link_to_route('pictures.index', 'Галерея') }}
                </li>
                @if(Sentry::getUser()->vk->id==139396231)
                <li
                    {{ Route::currentRouteName() == 'moder' ? 'class="active"' : '' }}>
                    {{ link_to_route('moder', 'Модерация') }}
                </li>
                @endif
        </ul>
            <div class="nav navbar-nav navbar-right">
                <li
                    {{ Route::currentRouteName() == 'help' ? 'class="active"' : '' }}>
                    {{ link_to_route('help', 'Помощь') }}
                </li>
                <li
                    {{ Route::currentRouteName() == 'work' ? 'class="active"' : '' }}>
                    {{ link_to_route('work', 'Работа') }}
                </li>
            </div>
        </div>
        <!--/.nav-collapse -->
    </div>
    <!--/.container-fluid -->
</div>