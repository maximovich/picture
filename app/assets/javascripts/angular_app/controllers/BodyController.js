'use strict';

pictureControllers.controller('BodyController',
    [
        '$scope',
        'Users',
        '$timeout',

        function ($scope, Users, $timeout) {

            $scope.authorApp = 'Vladimir Maximovich';
            $scope.userId = null;

            $scope.init = function (id, userId, first_name, last_name) {

                if (!first_name || !last_name) {

                    VK.api('users.get', {}, function (d) {
                        var first_name = d.response[0].first_name;
                        var last_name = d.response[0].last_name;

                        Users.update(
                            {
                                userId: id,
                                first_name: first_name,
                                last_name: last_name
                            }, function (d) {

                            }
                        );

                    });

                }

                $scope.userId = userId;

            };

            $scope.resize = function () {
                $timeout(function () {
                    var bw = $('body').width();
                    var bh = $('body').height();
                    VK.callMethod("resizeWindow", bw, bh);
                }, 100);
            };
        }
    ]
);
