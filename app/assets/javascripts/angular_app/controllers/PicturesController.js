'use strict';

pictureControllers.controller('PicturesController',
    [
        '$scope',
        'Pictures',
        'friendsModalService',

        function ($scope, Pictures, friendsModalService) {

            $scope.pictureId = null;
            $scope.textMsg = '';

            $scope.init = function (pictureId) {
                $scope.pictureId = pictureId;
            };

            $scope.sendMe = function () {
                $scope.sendWall($scope.userId);
            };

            $scope.sendFriend = function () {
                var clickOk = function (result) {
                    if (result) {
                        $scope.sendWall(result);
                    } else {
                        //$.notify('Произошла ошибка, повторите попытку позже', 'error');
                    }
                };

                var modalOptions = {
                    closeButtonText: 'Отмена',
                    actionButtonText: 'Выбрать',
                    clickOk: clickOk
                };

                friendsModalService.showModal({}, modalOptions);
            };

            $scope.sendWall = function (userId) {
                $('.overlay').show();
                VK.api("photos.getWallUploadServer", function (d) {
                    if (d.response && d.response.upload_url) {
                        Pictures.get(
                            {
                                pictureId: $scope.pictureId,
                                uploadUrl: d.response.upload_url

                            }, function (d) {
                                VK.api("photos.saveWallPhoto", {
                                    server: d.server,
                                    photo: d.photo,
                                    hash: d.hash,
                                    user_id: $scope.userId
                                }, function (d) {
                                    if (d.response[0].id) {

                                        var link = 'http://vk.com/app4569339';
                                        var message = $scope.textMsg; //+ '\r\n \r\n Большие откртыки - ' + link;
                                        var stringAttachments = '';


                                        for (var i = 0; i <= d.response.length - 1; i++) {
                                            stringAttachments += 'photo' + d.response[i].owner_id + '_' + d.response[i].id + ',';
                                        }

                                        stringAttachments += link;

                                        VK.api("wall.post", {
                                            owner_id: userId, //тут поменять на стену друга
                                            message: message,
                                            attachments: stringAttachments
                                        }, function (d) {
                                            $('.overlay').hide();
                                        }, function (d) {
                                            $('.overlay').hide();
                                            if (d.error.error_code == 214) {
                                                $.notify('Пользователь запретил вам размещать записи у себя на стене', 'error');
                                                return;
                                            } else if (d.error.error_code == 10007) {
                                                return;
                                            }
                                            $.notify('Произошла ошибка, повторите попытку позже', 'error');
                                        });
                                    } else {
                                        $('.overlay').hide();
                                        $.notify('Произошла ошибка, повторите попытку позже', 'error');
                                    }
                                });
                            }, function (d) {
                                $('.overlay').hide();
                                $.notify('Произошла ошибка, повторите попытку позже', 'error');
                            }
                        );
                    } else {
                        $('.overlay').hide();
                        $.notify('Произошла ошибка, необходим доступ к стене и доступ к фотографиям', 'error');
                    }
                });
            };

        }
    ]
);