<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePictureonesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'pictureones',
            function ( Blueprint $table ) {
                $table->increments( 'id' );
                $table->string( 'path' );
                $table->integer( 'picture_id' )->unsigned()->index();
                $table->timestamps();

                $table->foreign( 'picture_id' )
                    ->references( 'id' )
                    ->on( 'pictures' )
                    ->onDelete( 'cascade' )
                    ->onUpdate( 'cascade' );
            }
        );
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'pictureones' );
    }

}
