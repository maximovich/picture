<?php

namespace Api\v1;

use BaseController, Input, Response;
use Fitcom\Serializer\DataArraySerializer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class ApiController extends BaseController
{
    public function getCollection($data, $transformer, $key)
    {
        $manager = new Manager();

        if (Input::get('include')) {
            $manager->parseIncludes(Input::get('include'));
        }

        $manager->setSerializer(new DataArraySerializer());

        $resource = new Collection($data, $transformer, $key);

        return Response::json($manager->createData($resource)->toArray());
    }

    public function getItem($data, $transformer, $key)
    {
        $manager = new Manager();

        if (Input::get('include')) {
            $manager->parseIncludes(Input::get('include'));
        }

        $manager->setSerializer(new DataArraySerializer());

        $resource = new Item($data, $transformer, $key);

        return Response::json($manager->createData($resource)->toArray());
    }
}