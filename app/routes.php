<?php

Route::group(
    [ 'before' => 'user' ],
    function () {

        Route::get( '/', [ 'as' => 'home', 'uses' => 'HomeController@showWelcome' ] );
        Route::get( '/help', [ 'as' => 'help', 'uses' => 'HomeController@help' ] );
        Route::get( '/work', [ 'as' => 'work', 'uses' => 'HomeController@work' ] );
        Route::get( '/moder', [ 'as' => 'moder', 'uses' => 'HomeController@moder' ] );


        Route::resource( 'pictures', 'PicturesController' );
        Route::get(
            'pictures/category/{category?}',
            [ 'as' => 'pictures.category', 'uses' => 'PicturesController@index' ]
        );

    }
);

Route::group(
    [ 'before' => 'guest', 'prefix' => 'api/v1', 'namespace' => 'Api\v1' ],
    function () {
        Route::get( 'pictures/{pictureId}/like', [ 'as' => 'pictures.like', 'uses' => 'PicturesController@like' ] );
        Route::post(
            'pictures/{pictureId}/accept',
            [ 'as' => 'pictures.accept', 'uses' => 'PicturesController@accept' ]
        );
        Route::resource( 'pictures', 'PicturesController', [ 'only' => [ 'show', 'destroy' ] ] );
        Route::resource( 'users', 'UsersController', [ 'only' => [ 'update' ] ] );
    }
);