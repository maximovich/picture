'use strict';

/* App Module */

var picture = angular.module('picture',
    [
        'pictureControllers',
        'pictureServices',
        'ui.bootstrap'
    ]
).config(
    [
        '$httpProvider',

        function ($httpProvider) {
            $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name="csrf-token"]').attr('content');
            $httpProvider.interceptors.push(
                function ($q) {
                    return {
                        'request': function (config) {
                            $('.overlay').show();
                            return config;
                        },
                        'requestError': function (rejection) {
                            $('.overlay').show();
                            if (canRecover(rejection)) {
                                return responseOrNewPromise
                            }
                            return $q.reject(rejection);
                        },
                        'responseError': function (r) {
                            $('.overlay').hide();
                            $.notify('Произошла ошибка попробуйте обновить страницу');
                            return $q.reject(r);
                        },
                        'response': function (response) {
                            $('.overlay').hide();
                            return response;
                        }
                    }
                }
            );
        }
    ]
);

// controllers
var pictureControllers = angular.module('pictureControllers', []);

// services;
var pictureServices = angular.module('pictureServices', ['ngResource']);