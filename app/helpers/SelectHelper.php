<?php

class SelectHelper
{
    public static function getCategories()
    {
        $categoriesCollection = Category::all();

        if ( !$categoriesCollection->count() ) {
            return false;
        }

        $categories = [ ];

        foreach ( $categoriesCollection as $category ) {
            $categories[ $category->id ] = $category->title;
        }

        if ( !count( $categories ) ) {
            return false;
        }

        return $categories;

    }

}