<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLikesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'likes',
            function ( Blueprint $table ) {
                $table->increments( 'id' );
                $table->integer( 'user_id' )->unsigned()->index();
                $table->integer( 'picture_id' )->unsigned()->index();
                $table->timestamps();

                $table->foreign( 'user_id' )
                    ->references( 'id' )
                    ->on( 'users' )
                    ->onDelete( 'cascade' )
                    ->onUpdate( 'cascade' );

                $table->foreign( 'picture_id' )
                    ->references( 'id' )
                    ->on( 'pictures' )
                    ->onDelete( 'cascade' )
                    ->onUpdate( 'cascade' );
            }
        );
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'likes' );
    }

}
