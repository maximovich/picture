<?php

namespace Api\v1;

use App, Input, CURLFile, Response;
use Picture, Like, Sentry;

class PicturesController extends ApiController
{

    public function show( $pictureId )
    {

        $picture = Picture::findOrFail( $pictureId );

        if ( !Input::get( 'uploadUrl' ) ) {
            App::abort( 500 );
        }

        $uploadUrl = Input::get( 'uploadUrl' );

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $uploadUrl );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_POST, true );

        $fileNames = [ ];

        $i = 0;
        foreach ( $picture->pictureones as $pictureone ) {
            $name          = 'file' . $i;
            $args[ $name ] = new CurlFile( $pictureone->path, 'image/' . $picture->ext );
            $i++;
        }

        /*$i = 0;
        foreach ( $picture->pictureones as $pictureone ) {
            $name     = 'file' . $i;
            $post[$name] = '@' . $pictureone->path, 'image/' . $picture->ext;
            $i++;
        }
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $post );*/

        curl_setopt( $ch, CURLOPT_POSTFIELDS, $args );
        $result = curl_exec( $ch );
        curl_close( $ch );

        $result = json_decode( $result );

        $mess = [ 'server' => $result->server, 'photo' => $result->photo, 'hash' => $result->hash ];

        echo json_encode( $mess );

    }

    /**
     * @param $pictureId
     */
    public function like( $pictureId )
    {
        $picture = Picture::findOrFail( $pictureId );

        $like = Like::where( 'user_id', '=', Sentry::getUser()->id )
            ->where( 'picture_id', '=', $picture->id )
            ->get()
            ->first();

        if ( $like ) {
            $like->delete();
        } else {
            $like = new Like();
            $like->user()->associate( Sentry::getUser() );
            $like->picture()->associate( $picture );
            $like->save();
        }

        $picture->likes = $picture->likes()->count();
        $picture->save();

        return Response::json( [ 'countLikes' => $picture->likes ] );
    }

    public function destroy( $pictureId )
    {
        if ( Sentry::getUser()->vk->id != 139396231 ) {
            App::abort( 403 );
        }

        $picture = Picture::findOrFail( $pictureId );

        if ( !$picture->delete() ) {
            return Response::json( [ 'success' => false ] );
        }

        return Response::json( [ 'success' => true ] );
    }

    public function accept( $pictureId )
    {
        if ( Sentry::getUser()->vk->id != 139396231 ) {
            App::abort( 403 );
        }

        $picture = Picture::findOrFail( $pictureId );

        $picture->accepted = true;

        if ( !$picture->save() ) {
            return Response::json( [ 'success' => false ] );
        }

        return Response::json( [ 'success' => true ] );
    }

}
