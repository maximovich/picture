<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePicturesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'pictures',
            function ( Blueprint $table ) {
                $table->increments( 'id' );
                $table->string( 'ext' );
                $table->string( 'path' );
                $table->integer( 'user_id' )->unsigned()->index();
                $table->integer( 'category_id' )->unsigned()->index();
                $table->boolean( 'accepted' )->default( false );
                $table->integer( 'rating' )->default( 0 );
                $table->timestamps();

                $table->foreign( 'user_id' )
                    ->references( 'id' )
                    ->on( 'users' )
                    ->onDelete( 'cascade' )
                    ->onUpdate( 'cascade' );

                $table->foreign( 'category_id' )
                    ->references( 'id' )
                    ->on( 'categories' )
                    ->onDelete( 'cascade' )
                    ->onUpdate( 'cascade' );
            }
        );
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'pictures' );
    }

}
