'use strict';

pictureServices.factory('Pictures',
    [
        'pictureResource',
        'Config',

        function (pictureResource, Config) {
            return pictureResource(
                Config.prefixApi + 'pictures/:pictureId',
                {
                    pictureId: '@pictureId'
                },
                {
                    query: {
                        isArray: false
                    },
                    update: {
                        method: 'PUT',
                        isArray: false
                    },
                    like: {
                        method: 'GET',
                        url: Config.prefixApi + 'pictures/:pictureId/like',
                        isArray: false
                    },
                    accept: {
                        method: 'POST',
                        url: Config.prefixApi + 'pictures/:pictureId/accept',
                        isArray: false
                    }
                }
            );
        }
    ]
);
