<?php

use LaravelBook\Ardent\Ardent;

class Category extends Ardent
{
    protected $table = 'categories';

    public function pictures()
    {
        return $this->hasMany( 'Picture' );
    }

}