<?php

use LaravelBook\Ardent\Ardent;

class Picture extends Ardent
{
    protected $table = 'pictures';

    public function pictureones()
    {
        return $this->hasMany( 'Pictureone' );
    }

    public function user()
    {
        return $this->belongsTo( 'User' );
    }

    public function category()
    {
        return $this->belongsTo( 'Category' );
    }

    public function scopeSort( $query )
    {
        return $query->orderBy( 'accepted', 'desc' )->orderBy( 'likes', 'desc' )->orderBy( 'created_at', 'desc' );
    }

    public function likes()
    {
        return $this->hasMany( 'Like' );
    }
} 