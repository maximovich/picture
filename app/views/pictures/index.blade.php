@extends('layouts.main')

@section('title')Выбрать из галереи@stop

{{-- Content --}}
@section('content')
<div class="row" ng-controller="PicturesIndexController">

    <div class="col-sm-3">
        @include('pictures/_categories')
    </div>
    <div class="col-sm-9">


        <h1>
            @if(isset($category))
                {{$category->title }}
            @else
                Все открытки
            @endif
        </h1>
        <hr/>

        @include('pictures/_list')
    </div>
</div>

@stop