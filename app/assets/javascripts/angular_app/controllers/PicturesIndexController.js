'use strict';

pictureControllers.controller('PicturesIndexController',
    [
        '$scope',
        'Pictures',

        function ($scope, Pictures) {

            $scope.like = function (pictureId) {
                Pictures.like(
                    {
                        pictureId: pictureId
                    }, function (d) {
                        console.log(d);
                        if (d.countLikes == 0) {
                            $('#picture' + pictureId).html('');
                        } else {
                            $('#picture' + pictureId).html(d.countLikes);
                        }
                    }
                );

            };

        }
    ]
)
;