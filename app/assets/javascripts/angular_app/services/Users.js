'use strict';

pictureServices.factory('Users',
    [
        'pictureResource',
        'Config',

        function (pictureResource, Config) {
            return pictureResource(
                Config.prefixApi + 'users/:userId',
                {
                    userId: '@userId'
                },
                {
                    query: {
                        isArray: false
                    },
                    update: {
                        method: 'PUT',
                        isArray: false
                    }
                }
            );
        }
    ]
);
