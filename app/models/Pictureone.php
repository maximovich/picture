<?php

use LaravelBook\Ardent\Ardent;

class Pictureone extends Ardent
{
    protected $table = 'pictureones';

    public function picture()
    {
        return $this->belongsTo( 'Picture' );
    }
} 