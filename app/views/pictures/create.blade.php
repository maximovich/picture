@extends('layouts.main')

@section('title'){{ 'Создание новой открытки' }}@stop

@section('keywords'){{ 'Создание новой открытки' }}@stop

@section('description'){{ 'Создание новой открытки' }}@stop

@section('content')
<div class="page-header">
    <h3>Создание новой открытки</h3>
</div>

{{ Form::open(['route' => 'pictures.store', 'files' => true, 'class'=>'form-horizontal']) }}

<div class="form-group @if($errors->has('category_id')) {{ 'has-error' }} @endif">
    {{ Form::label('category_id', 'Категория *', ['class' => 'col-xs-2']) }}
    <div class="col-xs-10">
        {{ Form::select('category_id', $categories, Input::old('category_id') ? Input::old('category_id') : 1) }}
        {{ $errors->first('category_id', '<span class="help-block">:message</span>') }}
        <p class="help-block">Постарайтесь подобрать наиболее подходящую категорию. Открытка будет удалена если вы разместите её неправильно</p>
    </div>
</div>

<div class="form-group @if($errors->has('file')) {{ 'has-error' }} @endif">
    {{ Form::label('file', 'Изображение *', ['class' => 'col-xs-2']) }}
    <div class="col-xs-10">
        {{ Form::file('file') }}
        {{ $errors->first('file', '<span class="help-block">:message</span>') }}
    </div>
</div>


<div class="form-group">
    <div class="col-xs-offset-2 col-xs-10">
        {{ Form::submit('Создать', ['class' => 'btn-success'] ) }}
    </div>
</div>

{{ Form::close() }}
<br>
<br>
<br>
@stop