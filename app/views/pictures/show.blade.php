@extends('layouts.main')

@section('title')Отправить на стену@stop

@section('content')

<div ng-controller="PicturesController" ng-init="init({{$picture->id}})">

<div class="page-header">
    <h3>Отправить открытку</h3>
</div>

<div class="row">
    <div class="col-xs-4">
    </div>
    <div class="col-xs-4">
        <img class="img-responsive" src="{{'/'.$picture->path}}">
    </div>
    <div class="col-xs-4">
    </div>
</div>

<div class="row">
    <div class="col-xs-12 text-center">
        <h3>Подпись к открытки</h3>
    </div>
</div>

<div class="row">
    <div class="col-xs-3"></div>
    <div class="col-xs-6 text-center">
        <textarea ng-model="textMsg" style="width: 100%; height: 100px;" placeholder="Всего хорошего :)"></textarea>
    </div>
    <div class="col-xs-3"></div>
</div>

<div class="row">
    <div class="col-xs-12 text-center">
        <button ng-click="sendMe()" class="btn-success btn">К себе на стену</button>
        <button ng-click="sendFriend()" class="btn-success btn">К другу на стену</button>
    </div>
</div>

</div>


<br>
<br>
<br>
@stop