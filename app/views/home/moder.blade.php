@extends('layouts.main')

@section('title'){{ 'Работа в России' }}@stop

@section('keywords'){{ 'Работа в России' }}@stop

@section('description'){{ 'Работа в России' }}@stop

@section('content')
<div class="row" ng-controller="ModerController">
    <div class="col-xs-12">
        <div class="page-header">
            <h2>Изображения для проверки</h2>
        </div>

        @if($pictures->count())
            <div>
                <div class="row">
                            <div class="col-xs-12">
                                {{ $pictures->links() }}
                            </div>
                        </div>

                <div class="row">
                @foreach ($pictures as $picture)
                    <div class="col-xs-4" style="min-height: 400px;" id="picture{{$picture->id}}">
                        <div>
                            <a href="{{route('pictures.show', [$picture->id])}}">
                                <img class="img-responsive" src="{{'/'.$picture->path}}">
                            </a>
                        </div>
                        <div>
                            Разместил: <a target="_blank" href="http://vk.com/id{{$picture->user->vk->id}}">{{$picture->user->vk->first_name}} {{$picture->user->vk->last_name}}</a>
                        </div>
                        <div>
                            Категория: {{$picture->category->title}}
                        </div>
                        <div>
                            <button ng-click="acceptPicture({{$picture->id}})" class="btn btn-success">Принять</button>
                            <button ng-click="deletePicture({{$picture->id}})" class="btn btn-danger">Удалить</button>
                        </div>
                        <hr>
                    </div>
                @endforeach
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        {{ $pictures->links() }}
                    </div>
                </div>

            </div>
        @else
            Ничего нет
        @endif

    </div>
</div>
<br>
<br>
<br>
@stop