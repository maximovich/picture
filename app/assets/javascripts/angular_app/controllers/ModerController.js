'use strict';

pictureControllers.controller('ModerController',
    [
        '$scope',
        'Pictures',

        function ($scope, Pictures) {

            $scope.deletePicture = function (pictureId) {
                console.log(pictureId);
                Pictures.delete(
                    {
                        pictureId: pictureId
                    }, function (d) {
                        console.log(d);
                        if (d.success == true) {
                            $('#picture' + pictureId).remove();
                            $scope.resize();
                        }
                    }, function (d) {
                        $.notify('Произошла ошибка при удалении, повторите попытку позже', 'error');
                    }
                );
            };

            $scope.acceptPicture = function (pictureId) {
                Pictures.accept(
                    {
                        pictureId: pictureId
                    }, function (d) {
                        console.log(d);
                        if (d.success == true) {
                            $('#picture' + pictureId).remove();
                            $scope.resize();
                        }
                    }, function (d) {
                        $.notify('Произошла ошибка при одобрении, повторите попытку позже', 'error');
                    }
                );
            };

        }
    ]
);