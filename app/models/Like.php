<?php

use LaravelBook\Ardent\Ardent;

class Like extends Ardent
{
    protected $table = 'likes';

    public function user()
    {
        return $this->belongsTo( 'User' );
    }

    public function picture()
    {
        return $this->belongsTo( 'Picture' );
    }

}