<?php

class PicturesController extends BaseController
{
    public function index( $categoryId = null )
    {
        $categories = Category::all();

        if ( $categoryId == null ) {
            $pictures = Picture::sort()->paginate( 12 );

            return View::make( 'pictures.index', compact( 'pictures', 'categories' ) );
        } else {
            $category = Category::findOrFail( $categoryId );
            $pictures = $category->pictures()->sort()->paginate( 12 );
            return View::make( 'pictures.index', compact( 'pictures', 'category', 'categories' ) );
        }
    }

    public function create()
    {
        $categories = SelectHelper::getCategories();

        return View::make( 'pictures.create', compact( 'categories' ) );
    }

    public function store()
    {
        $validate = [
            'category_id' => Input::get( 'category_id' ),
            'file'        => Input::file( 'file' )
        ];

        $validateRules = [
            'category_id' => 'required|numeric|exists:categories,id',
            'file'        => 'required|image'
        ];

        $validator = Validator::make( $validate, $validateRules );

        if ( $validator->fails() ) {
            return Redirect::back()->withInput()->withErrors( $validator );
        }

        $destinationPath = 'images';

        if ( !is_dir( $destinationPath ) ) {
            mkdir( $destinationPath );
        }

        $destinationPath .= '/' . Sentry::getUser()->id;

        if ( !is_dir( $destinationPath ) ) {
            mkdir( $destinationPath );
        }

        $destinationPath .= '/' . Sentry::getUser()->id . str_random( 32 );

        if ( !is_dir( $destinationPath ) ) {
            mkdir( $destinationPath );
        }

        $file     = Input::file( 'file' );
        $fileExt  = $file->getClientOriginalExtension();
        $fileName = str_random( 32 ) . '.' . $fileExt;

        $fileUploadSuccess = $file->move( $destinationPath, $fileName );

        if ( !$fileUploadSuccess ) {
            return Redirect::back()->with( 'error', 'Произошла ошибка на повторите попытку' );
        }

        $image = Image::make( $destinationPath . '/' . $fileName )
            ->widen( 400 )
            ->save( $destinationPath . '/' . $fileName );


        $pathPicture = $destinationPath . '/' . $fileName;

        $destinationPath .= '/' . 'crop';

        if ( !is_dir( $destinationPath ) ) {
            mkdir( $destinationPath );
        }

        $height = $image->height();

        $x = 80;

        $pictureoneNames = [ ];

        for ( $i = 0; $i <= 4; $i++ ) {
            $image = Image::make( $pathPicture );
            $image->crop( 80, $height, $x * $i, 0 );
            $image->save( $destinationPath . '/' . $i . '.' . $fileExt );
            $pictureoneNames[ ] = $destinationPath . '/' . $i . '.' . $fileExt;
        }

        $picture       = new Picture;
        $picture->ext  = $fileExt;
        $picture->path = $pathPicture;
        $picture->user()->associate( Sentry::getUser() );
        $picture->category()->associate( Category::find( Input::get( 'category_id' ) ) );
        $picture->save();

        foreach ( $pictureoneNames as $pictureoneName ) {
            $pictureone       = new Pictureone;
            $pictureone->path = $pictureoneName;
            $pictureone->picture()->associate( $picture );
            $pictureone->save();
        }

        return Redirect::route( 'pictures.show', [ $picture->id ] );
    }

    public function show( $id )
    {
        $picture = Picture::findOrFail( $id );

        return View::make( 'pictures.show', compact( 'picture' ) );
    }

}