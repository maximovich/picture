@if($categories->count())

    <div>

        <p><h1>Категории</h1></p>
        <hr/>
        @foreach ($categories as $cat)

            <p>

                @if( isset($category) && $cat->id === $category->id )
                    <strong>{{$cat->title}}</strong>
                @else
                    {{link_to_route('pictures.category', $cat->title, [$cat->id])}}
                @endif

                <sup><strong>{{ count($cat->pictures) ? count($cat->pictures) : '' }}</strong></sup>
            </p>

            <hr />
        @endforeach
    </div>
@endif