'use strict';

pictureServices.factory('Abstract',
    [
        'pictureResource',
        'Config',

        function (pictureResource, Config) {
            return pictureResource(
                Config.prefixApi + 'abstracts/:abstractId',
                {
                    abstractId: '@abstractId'
                },
                {
                    query: {
                        isArray: false
                    },
                    update: {
                        method: 'PUT',
                        isArray: false
                    }
                }
            );
        }
    ]
);
