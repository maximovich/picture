'use strict';

pictureServices.service('friendsModalService',
    [
        '$modal',

        function ($modal) {

            var modalDefaults = {
                backdrop: true,
                keyboard: true,
                modalFade: true,
                templateUrl: '/templates/friendsModal.html',
                size: 'md'
            };

            var modalOptions = {
                closeButtonText: 'Отмена',
                actionButtonText: 'Выбрать',
                headerText: 'Выбрать друга'
            };


            this.showModal = function (customModalDefaults, customModalOptions) {

                if (!customModalDefaults) customModalDefaults = {};

                customModalDefaults.backdrop = 'static';

                var self = this;

                VK.api('friends.get', {
                    fields: 'uid, first_name, last_name, photo_100',
                    order: 'name'
                }, function (data) {
                    if (data.response) {
                        customModalOptions.friends = data.response.items;
                        self.show(customModalDefaults, customModalOptions);
                    }
                });


            };

            this.show = function (customModalDefaults, customModalOptions) {

                var tempModalDefaults = {};
                var tempModalOptions = {};

                angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

                angular.extend(tempModalOptions, modalOptions, customModalOptions);

                if (!tempModalDefaults.controller) {
                    tempModalDefaults.controller = function ($scope, $modalInstance) {

                        $scope.obj = {};
                        $scope.modalOptions = tempModalOptions;

                        $scope.selectFriend = function (friend) {
                            $('.friend').removeClass('selectedFriend');
                            $('#friend' + friend.id).addClass('selectedFriend');
                            $scope.obj.friendId = friend.id;
                        };

                        $scope.obj.friendId = 0;

                        $scope.modalOptions.ok = function (result) {
                            $scope.modalOptions.clickOk($scope.obj.friendId);
                            $modalInstance.close(result);
                        };

                        $scope.modalOptions.close = function (result) {
                            $scope.modalOptions.clickOk(false);
                            $modalInstance.dismiss('cancel');
                        };
                    }
                }

                return $modal.open(tempModalDefaults).result;
            };

        }
    ]
);