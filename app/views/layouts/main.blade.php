<!doctype html>
<html lang="ru" ng-app="picture">
@include('partials._meta')
<body ng-controller="BodyController"
ng-init="init({{Sentry::getUser()->id}}, {{Sentry::getUser()->vk->id}}, '{{Sentry::getUser()->vk->first_name}}', '{{Sentry::getUser()->vk->last_name}}')">
<div class="overlay">
@include('partials/_preloader')
</div>


<div class="container">

    @include('partials/_navbar')

    @include('partials/_notifications')

    @yield('content')

</div>

{{ javascript_include_tag() }}

@yield('scripts')

<script src="//vk.com/js/api/xd_connection.js?2"  type="text/javascript"></script>
<script type="text/javascript">
    $().ready(function(){
        VK.init(function() {
            var bw = $('body').width();
            var bh = $('body').height();

            VK.callMethod("scrollWindow", 0, 500);
            VK.callMethod("resizeWindow", bw, bh);

        }, function() {

        }, '5.24');
    });
</script>
</body>
</html>
